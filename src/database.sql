DROP DATABASE `university`;
CREATE DATABASE `university`;
USE `university`;

CREATE TABLE `cars` (
  `car_id`        INTEGER UNSIGNED            AUTO_INCREMENT,
  `license_plate` VARCHAR(10) UNIQUE NOT NULL,
  `model`         VARCHAR(50),
  `year`          SMALLINT UNSIGNED           DEFAULT 1970,
  `seats`         TINYINT UNSIGNED   NOT NULL DEFAULT 4,
  `trunk`         BIT(1)                      DEFAULT 0,
  `checked`       BIT(1)                      DEFAULT 1,
  PRIMARY KEY (`car_id`)
);

CREATE TABLE `driving_licenses` (
  `license_id`     INTEGER UNSIGNED AUTO_INCREMENT,
  `license_number` CHAR(8),
  `date_issued`    DATE,
  `date_expires`   DATE,
  PRIMARY KEY (`license_id`)
);

CREATE TABLE `clients` (
  `client_id`  INTEGER UNSIGNED AUTO_INCREMENT,
  `first_name` VARCHAR(50)      DEFAULT "",
  `last_name`  VARCHAR(50)      DEFAULT "",
  `egn`        CHAR(10)                NOT NULL UNIQUE,
  `phone`      VARCHAR(15)      DEFAULT "",
  `address`    VARCHAR(255)     DEFAULT "",
  `license_id` INTEGER UNSIGNED UNIQUE NOT NULL,
  PRIMARY KEY (`client_id`),
  KEY `i_clients_license_id` (`license_id`),
  CONSTRAINT `fk_clients_license_id` FOREIGN KEY (`license_id`) REFERENCES `driving_licenses` (`license_id`)
);

CREATE TABLE `orders` (
  `order_id`   INTEGER UNSIGNED AUTO_INCREMENT,
  `start_date` DATE,
  `end_date`   DATE,
  `price`      FLOAT UNSIGNED,
  `client_id`  INTEGER UNSIGNED NOT NULL,
  `car_id`     INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `i_orders_client_id` (`client_id`),
  KEY `i_orders_car_id` (`car_id`),
  CONSTRAINT `fk_orders_client_id` FOREIGN KEY (`client_id`) REFERENCES `clients` (`client_id`),
  CONSTRAINT `fk_orders_car_id` FOREIGN KEY (`car_id`) REFERENCES `cars` (`car_id`)
);

CREATE TABLE `users` (
  `user_id`  INTEGER UNSIGNED AUTO_INCREMENT,
  `role`     TINYINT UNSIGNED,
  `username` VARCHAR(40) NOT NULL,
  `password` VARCHAR(40) NOT NULL,
  PRIMARY KEY (`user_id`)
);

INSERT INTO `users` (`role`, `username`, `password`)
VALUES (1, 'admin', md5('admin'));

INSERT INTO `driving_licenses` (`license_number`, `date_issued`, `date_expires`)
VALUES ('25632145', '2008-05-12', '2018-05-12');
SELECT LAST_INSERT_ID()
INTO @xID;
INSERT INTO `clients` (`first_name`, `last_name`, `egn`, `phone`, `address`, `license_id`)
VALUES ('Theon', 'Greyjoy', '9536254789', '+359845513651', 'Some Street', @xID);
INSERT INTO `driving_licenses` (`license_number`, `date_issued`, `date_expires`)
VALUES ('32659789', '2008-05-12', '2018-05-12');
SELECT LAST_INSERT_ID()
INTO @xID;
INSERT INTO `clients` (`first_name`, `last_name`, `egn`, `phone`, `address`, `license_id`)
VALUES ('Arya', 'Stark', '8996532598', '+359845513651', 'Some Street', @xID);
INSERT INTO `driving_licenses` (`license_number`, `date_issued`, `date_expires`)
VALUES ('25418745', '2008-05-12', '2018-05-12');
SELECT LAST_INSERT_ID()
INTO @xID;
INSERT INTO `clients` (`first_name`, `last_name`, `egn`, `phone`, `address`, `license_id`)
VALUES ('Jaime', 'Lanister', '6789532598', '+359845513651', 'Some Street', @xID);
INSERT INTO `driving_licenses` (`license_number`, `date_issued`, `date_expires`)
VALUES ('45743652', '2008-05-12', '2018-05-12');
SELECT LAST_INSERT_ID()
INTO @xID;
INSERT INTO `clients` (`first_name`, `last_name`, `egn`, `phone`, `address`, `license_id`)
VALUES ('Tirion', 'Lannister', '4935532598', '+359845513651', 'Some Street', @xID);
INSERT INTO `driving_licenses` (`license_number`, `date_issued`, `date_expires`)
VALUES ('89745412', '2008-05-12', '2018-05-12');
SELECT LAST_INSERT_ID()
INTO @xID;
INSERT INTO `clients` (`first_name`, `last_name`, `egn`, `phone`, `address`, `license_id`)
VALUES ('Joffrey', 'Barateon', '2574532598', '+359845513651', 'Some Street', @xID);
INSERT INTO `driving_licenses` (`license_number`, `date_issued`, `date_expires`)
VALUES (54795476, '2008-05-12', '2018-05-12');
SELECT LAST_INSERT_ID()
INTO @xID;
INSERT INTO `clients` (`first_name`, `last_name`, `egn`, `phone`, `address`, `license_id`)
VALUES ('Catelyn', 'Stark', '4578532598', '+359845513651', 'Some Street', @xID);
INSERT INTO `driving_licenses` (`license_number`, `date_issued`, `date_expires`)
VALUES ('57953589', '2008-05-12', '2018-05-12');
SELECT LAST_INSERT_ID()
INTO @xID;
INSERT INTO `clients` (`first_name`, `last_name`, `egn`, `phone`, `address`, `license_id`)
VALUES ('Ned', 'Stark', '5364532598', '+359845513651', 'Some Street', @xID);
INSERT INTO `driving_licenses` (`license_number`, `date_issued`, `date_expires`)
VALUES ('12589654', '2008-05-12', '2018-05-12');
SELECT LAST_INSERT_ID()
INTO @xID;
INSERT INTO `clients` (`first_name`, `last_name`, `egn`, `phone`, `address`, `license_id`)
VALUES ('Daenerys', 'Targarien', '4789532598', '+359845513651', 'Some Street', @xID);
INSERT INTO `driving_licenses` (`license_number`, `date_issued`, `date_expires`)
VALUES ('65698575', '2008-05-12', '2018-05-12');
SELECT LAST_INSERT_ID()
INTO @xID;
INSERT INTO `clients` (`first_name`, `last_name`, `egn`, `phone`, `address`, `license_id`)
VALUES ('John', 'Snow', '9966532598', '+359845513651', 'Some Street', @xID);

INSERT INTO `cars`
(`license_plate`, `model`, `year`, `seats`, `trunk`, `checked`)
VALUES ('CA 3265 AH', 'Opel Vectra', 2003, 4, 1, 1),
  ('B 0025 AX', 'Nissan Micra', 2006, 4, 0, 1),
  ('H 9128 BC', 'Peugeot 206', 2008, 3, 0, 0),
  ('EH 6523 KA', 'Renault Megane', 2006, 9, 1, 1),
  ('A 3546 TA', 'Toyota Corolla', 2002, 5, 1, 1),
  ('PP 5465 AH', 'Citroen Xantia', 2004, 5, 1, 1),
  ('P 5452 AX', 'Mazda 6', 2005, 4, 0, 1),
  ('CB 3542 BC', 'Peugeot 307', 2009, 10, 0, 0),
  ('TX 3563 PA', 'Honda Civic', 1998, 6, 1, 1),
  ('A 5156 AT', 'Rover 25', 2001, 6, 1, 1);

INSERT INTO orders (`start_date`, `end_date`, `price`, `car_id`, `client_id`)
VALUES ('2017-01-01', '2017-01-09', 622.30, 1, 1),
  ('2017-01-01', '2017-01-09', 622.30, 1, 1),
  ('2015-10-01', '2015-12-09', 541.26, 3, 4),
  ('2016-09-10', '2016-09-18', 351.32, 9, 5),
  ('2015-01-01', '2015-01-09', 622.30, 3, 6);
