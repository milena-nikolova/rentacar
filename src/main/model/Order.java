package main.model;

import java.util.Date;

public class Order {
    private int id;
    private Date startDate;
    private Date endDate;
    private double price;
    private Client client;
    private Car car;

    public Order(int id, Date startDate, Date endDate, double price, Client client, Car car) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.price = price;
        this.client = client;
        this.car = car;
    }

    public Order(Date startDate, Date endDate, double price, Client client, Car car) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.price = price;
        this.client = client;
        this.car = car;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", price=" + price +
                ", client=" + client +
                ", car=" + car +
                '}';
    }
}
