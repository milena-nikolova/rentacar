package main.model;

import java.util.HashMap;
import java.util.Map;

public enum UserRoles {
    ADMIN(1), REGULAR_USER(2);

    private static final Map<Integer, UserRoles> intToTypeMap = new HashMap<>();

    static {
        for (UserRoles type : UserRoles.values()) {
            intToTypeMap.put(type.value, type);
        }
    }

    private int value;

    UserRoles(int i) {
        this.value = i;
    }

    public static UserRoles fromInt(int i) {
        UserRoles type = intToTypeMap.get(i);
        if (type == null)
            return UserRoles.REGULAR_USER;
        return type;
    }
}
