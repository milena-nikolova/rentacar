package main.model;


public class Client {
    private int id;
    private String firstName;
    private String lastName;
    private String egn;
    private String phone;
    private String address;
    private DrivingLicense license;

    public Client(int id, String firstName, String lastName, String egn, String phone, String address, DrivingLicense license) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.egn = egn;
        this.phone = phone;
        this.address = address;
        this.license = license;
    }

    public Client(String firstName, String lastName, String egn, String phone, String address, DrivingLicense license) {
        this.id = 0;
        this.firstName = firstName;
        this.lastName = lastName;
        this.egn = egn;
        this.phone = phone;
        this.address = address;
        this.license = license;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEgn() {
        return egn;
    }

    public void setEgn(String egn) {
        this.egn = egn;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public DrivingLicense getLicense() {
        return license;
    }

    @Override
    public String toString() {
        return firstName + ' ' + lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
