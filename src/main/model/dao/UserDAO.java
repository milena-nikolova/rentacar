package main.model.dao;

import main.model.DAOInterface;
import main.model.User;
import main.model.UserRoles;
import main.util.CustomAlert;
import main.util.DBUtilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserDAO implements DAOInterface<User> {
    @Override
    public boolean addEntry(User user) {
        try {
            DBUtilities.dbExecuteUpdate(
                    "INSERT INTO users (username, password, role) " +
                            "VALUES('" + user.getUsername() + "',md5('" + user.getPassword() + "')," + user.getRole().ordinal() + ");"
            );
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteEntry(int id) {
        try {
            DBUtilities.dbExecuteUpdate("DELETE FROM users WHERE user_id = " + id + ";");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean updateEntry(User obj) {
        return false;
    }

    @Override
    public ArrayList<User> getAllEntries() {
        return null;
    }

    @Override
    public User getEntry(int id) {
        return null;
    }


    public User getByUsername(String username) {
        User user = null;
        try {
            ResultSet rs = DBUtilities.dbExecuteQuery("SELECT * FROM users WHERE username = '" + username + "';");
            if (!rs.wasNull() && rs.next()) {
                user = new User(
                        rs.getInt(1),
                        rs.getString(3),
                        rs.getString(4),
                        UserRoles.fromInt(rs.getInt(2))
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
            CustomAlert.error("SQL Error");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return user;
    }

    public boolean comparePassword(int id, String password) {
        try {
            ResultSet rs = DBUtilities.dbExecuteQuery(
                    "SELECT * FROM users WHERE user_id = '" + id + "' AND password = md5('" + password + "');"
            );

            return !rs.wasNull() && rs.next();
        } catch (Exception e) {
            return false;
        }
    }
}
