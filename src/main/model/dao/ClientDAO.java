package main.model.dao;

import main.model.Client;
import main.model.DAOInterface;
import main.model.DrivingLicense;
import main.util.DBUtilities;

import java.sql.ResultSet;
import java.util.ArrayList;

public class ClientDAO implements DAOInterface<Client> {

    @Override
    public boolean addEntry(Client client) {
        try {
            DBUtilities.dbExecuteUpdate(
                    "INSERT INTO driving_licenses (license_number, date_issued, date_expires) values(" +
                            client.getLicense().getLicenseNumber() + ",'" +
                            DBUtilities.toSQLDate(client.getLicense().getDateIssued()) + "','" +
                            DBUtilities.toSQLDate(client.getLicense().getDateExpires()) + "');" +
                            "SELECT LAST_INSERT_ID() INTO @xId;" +
                            "INSERT INTO clients (first_name, last_name, egn, phone, address, license_id) values('" +
                            client.getFirstName() + "','" + client.getLastName() + "','" + client.getEgn() + "','" +
                            client.getPhone() + "', '" + client.getAddress() + "', @xId);"
            );
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Client getEntry(int id) {
        Client client = null;
        try {
            ResultSet rs = DBUtilities.dbExecuteQuery(
                    "SELECT client_id, first_name, last_name, egn, phone, address, dl.license_id, license_number, dl.date_issued, dl.date_expires " +
                            "FROM clients c JOIN driving_licenses dl ON c.license_id = dl.license_id WHERE client_id = " + id + ";"
            );
            if (!rs.wasNull() && rs.next()) {
                client = new Client(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        new DrivingLicense(rs.getInt(7), rs.getString(8), rs.getDate(9), rs.getDate(10))
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return client;
    }

    @Override
    public boolean deleteEntry(int id) {
        try {
            DBUtilities.dbExecuteUpdate(
                    "DELETE c, dl FROM clients c JOIN driving_licenses dl ON c.license_id = dl.license_id " +
                            "WHERE client_id = " + id + "; " +
                            "DELETE FROM orders WHERE client_id = " + id + ";"
            );
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean updateEntry(Client client) {
        try {
            DBUtilities.dbExecuteUpdate(
                    "UPDATE clients " +
                            "SET first_name = '" + client.getFirstName() + "', " +
                            "last_name = '" + client.getFirstName() + "', " +
                            "egn = '" + client.getEgn() + "', " +
                            "phone = '" + client.getPhone() + "', " +
                            "address = '" + client.getAddress() + "' " +
                            "WHERE client_id = " + client.getId() + ";" +

                            "UPDATE driving_licenses " +
                            "SET license_number = '" + client.getLicense().getLicenseNumber() + "', " +
                            "date_issued = '" + DBUtilities.toSQLDate(client.getLicense().getDateIssued()) + "'," +
                            "date_expires = '" + DBUtilities.toSQLDate(client.getLicense().getDateExpires()) + "' " +
                            "WHERE license_id = " + client.getLicense().getId() + ";"
            );
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public ArrayList<Client> getAllEntries() {
        try {
            ResultSet rs = DBUtilities.dbExecuteQuery(
                    "SELECT client_id, first_name, last_name, egn, phone, address, c.license_id, license_number, date_issued, date_expires " +
                            "FROM clients c JOIN driving_licenses dl ON c.license_id = dl.license_id"
            );
            ArrayList<Client> clients = new ArrayList<>();

            while (rs.next()) {
                clients.add(
                        new Client(
                                rs.getInt(1),
                                rs.getString(2),
                                rs.getString(3),
                                rs.getString(4),
                                rs.getString(5),
                                rs.getString(6),
                                new DrivingLicense(rs.getInt(7), rs.getString(8), rs.getDate(9), rs.getDate(10))
                        )
                );
            }

            return clients;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public ArrayList<Client> filterEntries(String firstName, String lastName, String egn, int licenseNumber) {
        try {
            ResultSet rs = DBUtilities.dbExecuteQuery(
                    "SELECT client_id, first_name, last_name, egn, phone, address, c.license_id, license_number, date_issued, date_expires " +
                            "FROM clients c JOIN driving_licenses dl ON c.license_id = dl.license_id " +
                            "WHERE first_name like '%" + firstName + "%' " +
                            "AND last_name like '%" + lastName + "%' " +
                            "AND ('" + egn + "' = '' OR egn = '" + egn + "') " +
                            "AND (" + licenseNumber + " = 0 OR license_number = " + licenseNumber + ");"
            );
            ArrayList<Client> clients = new ArrayList<>();

            while (rs.next()) {
                clients.add(
                        new Client(
                                rs.getInt(1),
                                rs.getString(2),
                                rs.getString(3),
                                rs.getString(4),
                                rs.getString(5),
                                rs.getString(6),
                                new DrivingLicense(rs.getInt(7), rs.getString(8), rs.getDate(9), rs.getDate(10))
                        )
                );
            }

            return clients;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public boolean checkEGN(String egn, int excludeID) {
        try {
            ResultSet rs = DBUtilities.dbExecuteQuery("SELECT count(*) FROM clients WHERE egn = '" + egn
                    + "' AND client_id <> +" + excludeID + ";");
            rs.next();
            return rs.getInt(1) == 0;
        } catch (Exception e) {
            return false;
        }
    }
}
