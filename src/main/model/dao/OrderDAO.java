package main.model.dao;

import main.model.DAOInterface;
import main.model.Order;
import main.util.DBUtilities;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

public class OrderDAO implements DAOInterface<Order> {
    private CarDAO carDAO = new CarDAO();
    private ClientDAO clientDAO = new ClientDAO();

    @Override
    public boolean addEntry(Order order) {
        try {
            DBUtilities.dbExecuteUpdate(
                    "INSERT INTO orders SET " +
                            "start_date = '" + DBUtilities.toSQLDate(order.getStartDate()) + "', " +
                            "end_date = '" + DBUtilities.toSQLDate(order.getEndDate()) + "', " +
                            "price = " + order.getPrice() + ", " +
                            "client_id = " + order.getClient().getId() + ", " +
                            "car_id = " + order.getCar().getId() + ";"
            );
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteEntry(int id) {
        try {
            DBUtilities.dbExecuteUpdate("DELETE FROM orders WHERE order_id = " + id + ";");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean updateEntry(Order order) {
        try {
            DBUtilities.dbExecuteUpdate(
                    "UPDATE orders SET " +
                            "start_date = '" + DBUtilities.toSQLDate(order.getStartDate()) + "', " +
                            "end_date = '" + DBUtilities.toSQLDate(order.getEndDate()) + "', " +
                            "price = " + order.getPrice() + ", " +
                            "client_id = " + order.getClient().getId() + ", " +
                            "car_id = " + order.getCar().getId() + " " +
                            "WHERE order_id = " + order.getId() + ";"
            );
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public ArrayList<Order> getAllEntries() {
        try {
            ResultSet rs = DBUtilities.dbExecuteQuery("SELECT order_id, start_date, end_date, price, client_id, car_id FROM orders");
            ArrayList<Order> orders = new ArrayList<>();

            while (rs.next()) {
                orders.add(new Order(
                                rs.getInt(1),
                                rs.getDate(2),
                                rs.getDate(3),
                                rs.getDouble(4),
                                this.clientDAO.getEntry(rs.getInt(5)),
                                this.carDAO.getEntry(rs.getInt(6))
                        )
                );
            }

            return orders;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    @Override
    public Order getEntry(int id) {
        Order order = null;
        try {
            ResultSet rs = DBUtilities.dbExecuteQuery("SELECT order_id, start_date, end_date, price, client_id, car_id FROM orders");

            if (!rs.wasNull() && rs.next()) {
                order = new Order(
                        rs.getInt(1),
                        rs.getDate(2),
                        rs.getDate(3),
                        rs.getDouble(4),
                        this.clientDAO.getEntry(rs.getInt(5)),
                        this.carDAO.getEntry(rs.getInt(6))
                );
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return order;
    }

    public ArrayList<Order> filterEntries(String modelPart, String namePart, Date fromDate) {
        try {
            ResultSet rs = DBUtilities.dbExecuteQuery(
                    "SELECT o.order_id, o.start_date, o.end_date, price, o.client_id, o.car_id FROM orders o " +
                            "JOIN cars c ON c.car_id = o.car_id JOIN clients cl ON cl.client_id = o.client_id " +
                            "WHERE c.model LIKE '%" + modelPart + "%' " +
                            "AND (cl.first_name LIKE '%" + namePart + "%' OR cl.last_name LIKE '%" + namePart + "%') " +
                            "AND o.start_date > " + DBUtilities.toSQLDate(fromDate) + ";");
            ArrayList<Order> orders = new ArrayList<>();

            while (rs.next()) {
                orders.add(new Order(
                                rs.getInt(1),
                                rs.getDate(2),
                                rs.getDate(3),
                                rs.getDouble(4),
                                this.clientDAO.getEntry(rs.getInt(5)),
                                this.carDAO.getEntry(rs.getInt(6))
                        )
                );
            }

            return orders;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public int getOrdersCountForCar (int carID) {
        try {
            ResultSet rs = DBUtilities.dbExecuteQuery("SELECT COUNT(*) FROM orders WHERE car_id = " + carID + ";");
            if (!rs.wasNull() && rs.next()) {
                return rs.getInt(1);
            } else {
                return 0;
            }
        } catch (Exception e) {
            return 0;
        }
    }

    public int getOrdersCountForClient (int clientID) {
        try {
            ResultSet rs = DBUtilities.dbExecuteQuery("SELECT COUNT(*) FROM orders WHERE client_id = " + clientID + ";");
            if (!rs.wasNull() && rs.next()) {
                return rs.getInt(1);
            } else {
                return 0;
            }
        } catch (Exception e) {
            return 0;
        }
    }

    public double getAveragePriceForCar (int carID) {
        try {
            ResultSet rs = DBUtilities.dbExecuteQuery("SELECT SUM(price) FROM orders WHERE car_id = " + carID + ";");
            if (!rs.wasNull() && rs.next()) {
                return rs.getDouble(1);
            } else {
                return 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
