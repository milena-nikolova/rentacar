package main.model.dao;

import main.model.Car;
import main.model.DAOInterface;
import main.util.DBUtilities;

import java.sql.ResultSet;
import java.util.ArrayList;

public class CarDAO implements DAOInterface<Car> {

    @Override
    public Car getEntry(int id) {
        Car car = null;
        try {
            ResultSet rs = DBUtilities.dbExecuteQuery(
                    "SELECT car_id, model, year, license_plate, seats, trunk, checked FROM cars WHERE car_id =" + id + ";"
            );
            if (!rs.wasNull() && rs.next()) {
                car = new Car(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getInt(5), rs.getBoolean(6), rs.getBoolean(7));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return car;
    }

    @Override
    public boolean addEntry(Car car) {
        try {
            DBUtilities.dbExecuteUpdate(
                    "INSERT INTO cars (model, year, license_plate, seats, trunk, checked) " +
                            "VALUES('" + car.getModel() + "', " + car.getYear() + ", '" +
                            car.getLicensePlate() + "', " +
                            car.getSeats() + ", " + car.hasTrunk() + ", " + car.isChecked() + ");"
            );
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean deleteEntry(int id) {
        try {
            DBUtilities.dbExecuteUpdate(
                    "DELETE FROM cars WHERE car_id = " + id + "; " +
                            "DELETE FROM orders WHERE car_id = " + id + ";"
            );
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean updateEntry(Car car) {
        try {
            DBUtilities.dbExecuteUpdate(
                    "UPDATE cars " +
                            "SET model = '" + car.getModel() + "', " +
                            "year = '" + car.getYear() + "', " +
                            "license_plate = '" + car.getLicensePlate() + "', " +
                            "seats = " + car.getSeats() + ", " +
                            "trunk = " + car.hasTrunk() + ", " +
                            "checked = " + car.isChecked() + " " +
                            "WHERE car_id = " + car.getId() + ";"
            );
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public ArrayList<Car> getAllEntries() {
        try {
            ResultSet rs = DBUtilities.dbExecuteQuery("SELECT car_id, model, year, license_plate, seats, trunk, checked FROM cars");
            ArrayList<Car> cars = new ArrayList<>();

            while (rs.next()) {
                cars.add(new Car(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getInt(5), rs.getBoolean(6), rs.getBoolean(7)));
            }

            return cars;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public ArrayList<Car> filterEntries(String model, int year, String licensePlate, boolean hasTrunk, boolean isChecked) {
        try {
            ResultSet rs = DBUtilities.dbExecuteQuery(
                    "SELECT car_id, model, year, license_plate, seats, trunk, checked FROM cars " +
                            "WHERE model like '%" + model + "%' " +
                            "AND year > " + year + " " +
                            "AND ('" + licensePlate + "' = '' OR license_plate = '" + licensePlate + "') " +
                            "AND trunk = " + hasTrunk + " AND checked = " + isChecked + ";");
            ArrayList<Car> cars = new ArrayList<>();

            while (rs.next()) {
                cars.add(new Car(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getInt(5), rs.getBoolean(6), rs.getBoolean(7)));
            }

            return cars;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public boolean checkLicensePlate(String licensePlate, int excludeID) {
        try {
            ResultSet rs = DBUtilities.dbExecuteQuery("SELECT count(*) FROM cars WHERE lcase(license_plate) = '"
                    + licensePlate.toLowerCase() + "' AND car_id <> " + excludeID + ";");
            rs.next();
            return rs.getInt(1) == 0;
        } catch (Exception e) {
            return false;
        }
    }
}
