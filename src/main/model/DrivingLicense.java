package main.model;

import java.util.Date;

public class DrivingLicense {
    private int id;
    private String licenseNumber;
    private Date dateIssued;
    private Date dateExpires;

    public DrivingLicense(int id, String licenseNumber, Date dateIssued, Date dateExpires) {
        this.id = id;
        this.licenseNumber = licenseNumber;
        this.dateIssued = dateIssued;
        this.dateExpires = dateExpires;
    }

    public DrivingLicense(String licenseNumber, Date dateIssued, Date dateExpires) {
        this.id = 0;
        this.licenseNumber = licenseNumber;
        this.dateIssued = dateIssued;
        this.dateExpires = dateExpires;
    }

    public Date getDateIssued() {
        return dateIssued;
    }

    public void setDateIssued(Date dateIssued) {
        this.dateIssued = dateIssued;
    }

    public Date getDateExpires() {
        return dateExpires;
    }

    public void setDateExpires(Date dateExpires) {
        this.dateExpires = dateExpires;
    }

    @Override
    public String toString() {
        return "DrivingLicense{" +
                "dateIssued=" + dateIssued +
                ", dateExpires=" + dateExpires +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }
}
