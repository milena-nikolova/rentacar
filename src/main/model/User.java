package main.model;

public class User {
    private int id = 0;
    private String username;
    private String password;
    private UserRoles role;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.role = UserRoles.REGULAR_USER;
    }

    public User(String username, String password, UserRoles role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public User(int id, String username, String password, UserRoles role) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRoles getRole() {
        return role;
    }

    public void setRole(UserRoles role) {
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
