package main.model;

import java.util.ArrayList;

public interface DAOInterface<T> {
    boolean addEntry(T obj);
    boolean deleteEntry(int id);
    boolean updateEntry(T obj);
    ArrayList<T> getAllEntries();
    T getEntry(int id);
}
