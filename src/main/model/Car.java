package main.model;

public class Car {
    private int id;
    private String model;
    private int year;
    private String licensePlate;
    private int seats;
    private boolean hasTrunk;
    private boolean isChecked;

    public Car() {
        this.id = 0;
        this.model = "";
        this.year = 1900;
        this.licensePlate = "";
        this.seats = 0;
        this.hasTrunk = false;
        this.isChecked = false;
    }

    public Car(int id, String model, int year, String licensePlate, int seats, boolean hasTrunk, boolean isChecked) {
        this.id = id;
        this.model = model;
        this.year = year;
        this.licensePlate = licensePlate;
        this.seats = seats;
        this.hasTrunk = hasTrunk;
        this.isChecked = isChecked;
    }

    public Car(String model, int year, String licensePlate, int seats, boolean hasTrunk, boolean isChecked) {
        this.id = 0;
        this.model = model;
        this.year = year;
        this.licensePlate = licensePlate;
        this.seats = seats;
        this.hasTrunk = hasTrunk;
        this.isChecked = isChecked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public boolean hasTrunk() {
        return hasTrunk;
    }

    public void setHasTrunk(boolean hasTrunk) {
        this.hasTrunk = hasTrunk;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    @Override
    public String toString() {
        return model + ", " + year + ", " + licensePlate;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
