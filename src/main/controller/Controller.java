package main.controller;

import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

public abstract class Controller implements Initializable {
    public MenuItem carsMenuItem;
    public MenuItem clientsMenuItem;
    public MenuItem ordersMenuItem;

    public MenuItem filterMenuItem;
    public MenuItem filterClientsMenuItem;
    public MenuItem filterCarsMEnuItem;

    public MenuItem carStatsMenuItem;
    public MenuItem clientStatsMenuItem;
    public MenuItem orderStatsMenuItem;

    public abstract Stage getStage();
}
