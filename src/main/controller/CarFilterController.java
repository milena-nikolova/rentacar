package main.controller;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import main.model.Car;
import main.model.dao.CarDAO;
import main.util.CustomAlert;
import main.util.GUIUtilities;

import java.net.URL;
import java.util.ResourceBundle;

public class CarFilterController extends Controller {
    private CarDAO carDAO = new CarDAO();

    @FXML
    private TableView<Car> carTable;
    @FXML
    private TableColumn<Car, String> modelCol;
    @FXML
    private TableColumn<Car, Integer> yearCol;
    @FXML
    private TableColumn<Car, String> licensePlateCol;
    @FXML
    private TableColumn<Car, Integer> seatsCol;
    @FXML
    private TableColumn<Car, Boolean> hasTrunkCol;
    @FXML
    private TableColumn<Car, Boolean> isCheckedCol;

    @FXML
    private TextField licensePlate;
    @FXML
    private TextField model;
    @FXML
    private TextField year;
    @FXML
    private CheckBox hasTrunk;
    @FXML
    private CheckBox isChecked;
    @FXML
    private Button searchButton;
    @FXML
    private Button clearButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        modelCol.setCellValueFactory(new PropertyValueFactory<>("model"));
        yearCol.setCellValueFactory(new PropertyValueFactory<>("year"));
        licensePlateCol.setCellValueFactory(new PropertyValueFactory<>("licensePlate"));
        seatsCol.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getSeats()));
        hasTrunkCol.setCellValueFactory(cellData -> new SimpleBooleanProperty(cellData.getValue().hasTrunk()));
        isCheckedCol.setCellValueFactory(cellData -> new SimpleBooleanProperty(cellData.getValue().isChecked()));

        GUIUtilities.setMaxLenght(year, 4);
        GUIUtilities.setMaxLenght(model, 50);
        GUIUtilities.setMaxLenght(licensePlate, 10);
        GUIUtilities.acceptIntegersOnly(year);

        searchButton.setOnAction(event -> {
            if (model.getText() != null && year.getText() != null && licensePlate.getText() != null &&
                    (licensePlate.getText().isEmpty() || licensePlate.getText().matches("^[A-Z]{1,2}\\s\\d{4}\\s[A-Z]{2}"))) {
                carTable.setItems(FXCollections.observableArrayList(
                        carDAO.filterEntries(
                                !model.getText().isEmpty() ? model.getText() : "",
                                !year.getText().isEmpty() ? Integer.valueOf(year.getText()) : 0,
                                !licensePlate.getText().isEmpty() ? licensePlate.getText() : "",
                                hasTrunk.isSelected(),
                                isChecked.isSelected()
                        )
                ));
            } else {
                CustomAlert.error("Some of the parameters are wrong.");
            }
        });

        clearButton.setOnAction(event -> {
            clearForm();
            carTable.setItems(null);
        });

        GUIUtilities.addMenuSupport(this);
    }

    private void clearForm() {
        licensePlate.clear();
        model.clear();
        year.clear();
        hasTrunk.setSelected(false);
        isChecked.setSelected(false);
    }

    @Override
    public Stage getStage() {
        return (Stage) this.clearButton.getScene().getWindow();
    }
}
