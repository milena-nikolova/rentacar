package main.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.stage.Stage;
import main.model.Car;
import main.model.dao.CarDAO;
import main.model.dao.OrderDAO;
import main.util.GUIUtilities;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class CarStatsController extends Controller {
    @FXML
    private PieChart pieChart;

    @Override
    public Stage getStage() {
        return (Stage) pieChart.getScene().getWindow();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        pieChart.setData(getCarUsageStatistics());
        pieChart.setTitle("Car Usage Statistics");

        GUIUtilities.addMenuSupport(this);
    }

    private ObservableList<PieChart.Data> getCarUsageStatistics() {
        OrderDAO orderDAO = new OrderDAO();
        ArrayList<Car> allCars = new CarDAO().getAllEntries();
        ArrayList<PieChart.Data> data = new ArrayList<>();
        for (Car car : allCars) {
            data.add(new PieChart.Data(car.toString(), orderDAO.getOrdersCountForCar(car.getId())));
        }
        return FXCollections.observableArrayList(data);
    }
}
