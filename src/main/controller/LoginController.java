package main.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.Main;
import main.model.User;
import main.model.dao.UserDAO;
import main.util.CustomAlert;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class LoginController implements Initializable {
    private UserDAO userDAO = new UserDAO();

    @FXML
    private TabPane tabPane;
    @FXML
    private TextField username;
    @FXML
    private PasswordField password;
    @FXML
    private Button loginButton;

    @FXML
    private TextField newUsername;
    @FXML
    private PasswordField newPassword;
    @FXML
    private PasswordField confirmNewPassword;
    @FXML
    private Button registerButton;
    @FXML
    private Button clearButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loginButton.setOnAction(event -> {
            if ((username.getText() != null && !username.getText().isEmpty()) &&
                    (password.getText() != null && !password.getText().isEmpty())) {

                User user = this.userDAO.getByUsername(username.getText());

                if (user != null && userDAO.comparePassword(user.getId(), password.getText())) {
                    Stage stage;
                    Parent root;

                    stage = (Stage) registerButton.getScene().getWindow();
                    try {
                        root = FXMLLoader.load(Main.class.getResource("view/car.fxml"));
                        Scene scene = new Scene(root);
                        stage.setScene(scene);
                        stage.show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    CustomAlert.error("Wrong username or password!");
                }

            } else {
                CustomAlert.error("Some of the fields are empty");
            }
        });

        registerButton.setOnAction(event -> {
            if ((newUsername.getText() != null && !newUsername.getText().isEmpty()) &&
                    (newPassword.getText() != null && !newPassword.getText().isEmpty()) &&
                    (confirmNewPassword.getText() != null && !confirmNewPassword.getText().isEmpty())) {

                if (!newPassword.getText().equals(confirmNewPassword.getText())) {
                    CustomAlert.error("Password doesn't match");
                } else if (this.userDAO.getByUsername(newUsername.getText()) != null) {
                    CustomAlert.error("This username is taken.");
                } else {
                    User user = new User(newUsername.getText(), newPassword.getText());
                    if (this.userDAO.addEntry(user)) {
                        CustomAlert.info("User created successfully. You can log in with your credentials.");
                        tabPane.getSelectionModel().select(0);
                    } else {
                        CustomAlert.error("Some error occurred.");
                    }
                }

            } else {
                CustomAlert.error("Some of the fields are empty");
            }
        });

        clearButton.setOnAction(event -> {
            newUsername.clear();
            newPassword.clear();
            confirmNewPassword.clear();
        });
    }
}
