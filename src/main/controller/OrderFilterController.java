package main.controller;

import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import main.model.Order;
import main.model.dao.OrderDAO;
import main.util.CustomAlert;
import main.util.GUIUtilities;

import java.net.URL;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;

public class OrderFilterController extends Controller {
    private OrderDAO orderDAO = new OrderDAO();

    @FXML
    private TableView<Order> orderTable;
    @FXML
    private TableColumn<Order, String> carCol;
    @FXML
    private TableColumn<Order, String> clientCol;
    @FXML
    private TableColumn<Order, Date> startDateCol;
    @FXML
    private TableColumn<Order, Date> endDateCol;
    @FXML
    private TableColumn<Order, Double> priceCol;

    @FXML
    private DatePicker afterDate;
    @FXML
    private TextField carModel;
    @FXML
    private TextField clientName;
    @FXML
    private Button searchButton;
    @FXML
    private Button clearButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        carCol.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getCar().toString()));
        clientCol.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getClient().toString()));
        priceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        startDateCol.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getStartDate()));
        endDateCol.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getEndDate()));
        searchButton.setOnAction(event -> {
            if (afterDate.getValue() == null) {
                CustomAlert.error("The date is required");
            } else {
                orderTable.setItems(FXCollections.observableArrayList(
                        orderDAO.filterEntries(
                                carModel.getText() != null ? carModel.getText() : "",
                                clientName.getText() != null ? clientName.getText() : "",
                                Date.from(afterDate.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant())
                        )
                ));
            }
        });

        clearButton.setOnAction(event -> {
            clearForm();
            orderTable.setItems(null);
        });

        GUIUtilities.addMenuSupport(this);
    }

    private void clearForm() {
        afterDate.getEditor().clear();
        carModel.clear();
        clientName.clear();
    }

    @Override
    public Stage getStage() {
        return (Stage) this.clearButton.getScene().getWindow();
    }
}
