package main.controller;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import main.model.Car;
import main.model.Client;
import main.model.Order;
import main.model.dao.CarDAO;
import main.model.dao.ClientDAO;
import main.model.dao.OrderDAO;
import main.util.CustomAlert;
import main.util.GUIUtilities;

import java.net.URL;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;

public class OrderController extends Controller {
    private int orderID = 0;
    private OrderDAO orderDAO = new OrderDAO();
    private ClientDAO clientDAO = new ClientDAO();
    private CarDAO carDAO = new CarDAO();

    @FXML
    private TableColumn<Order, String> carCol;
    @FXML
    private TableColumn<Order, String> clientCol;
    @FXML
    private TableColumn<Order, Date> startDateCol;
    @FXML
    private TableColumn<Order, Date> endDateCol;
    @FXML
    private TableColumn<Order, Double> priceCol;
    @FXML
    private TableColumn<Order, Order> editCol;
    @FXML
    private TableColumn<Order, Order> deleteCol;
    @FXML
    private TableView<Order> orderTable;

    @FXML
    private ComboBox<Client> client;
    @FXML
    private ComboBox<Car> car;
    @FXML
    private DatePicker startDate;
    @FXML
    private DatePicker endDate;
    @FXML
    private TextField price;

    @FXML
    private Button saveButton;
    @FXML
    private Button clearButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        carCol.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getCar().toString()));
        clientCol.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getClient().toString()));
        priceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        startDateCol.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getStartDate()));
        endDateCol.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getEndDate()));
        editCol.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        editCol.setCellFactory(param -> new TableCell<Order, Order>() {
            private final Button editButton = new Button("Edit");

            @Override
            protected void updateItem(Order order, boolean empty) {
                super.updateItem(order, empty);

                if (order == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(editButton);
                editButton.setOnAction(event -> {
                    orderID = order.getId();
                    price.setText(String.valueOf(order.getPrice()));
                    client.getSelectionModel().select(order.getClient());
                    car.getSelectionModel().select(order.getCar());
                    startDate.setValue(GUIUtilities.toLocalDate(order.getStartDate().toString()));
                    endDate.setValue(GUIUtilities.toLocalDate(order.getEndDate().toString()));
                });
            }
        });
        deleteCol.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        deleteCol.setCellFactory(param -> new TableCell<Order, Order>() {
            private final Button deleteButton = new Button("Delete");

            @Override
            protected void updateItem(Order order, boolean empty) {
                super.updateItem(order, empty);

                if (order == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(deleteButton);
                deleteButton.setOnAction(event -> {
                    getTableView().getItems().remove(order);
                    if (orderDAO.deleteEntry(order.getId())) {
                        CustomAlert.info("The selected order was deleted successfully.");
                    } else {
                        CustomAlert.error("Some error occurred.");
                    }
                });
            }
        });

        orderTable.setItems(FXCollections.observableArrayList(orderDAO.getAllEntries()));

        /*------------------------------------------------------------------------------------------------------------*/

        client.getItems().addAll(clientDAO.getAllEntries());
        car.getItems().addAll(carDAO.getAllEntries());
        GUIUtilities.acceptDoubleOnly(price);
        GUIUtilities.setMaxLenght(price, 8);

        saveButton.setOnAction(event -> {
            if ((client.getSelectionModel().getSelectedItem() != null) &&
                    (car.getSelectionModel().getSelectedItem() != null) &&
                    (price.getText() != null && !price.getText().isEmpty()) &&
                    (startDate.getValue() != null && endDate.getValue() != null)) {

                if (!GUIUtilities.isNumeric(price.getText())) {
                    CustomAlert.error("Invalid price value.");
                } else if (startDate.getValue().isAfter(endDate.getValue())) {
                    CustomAlert.error("The start date must be prior to the end date.");
                } else {
                    if (orderID > 0) {
                        Order order = orderDAO.getEntry(orderID);
                        order.setPrice(Double.valueOf(price.getText()));
                        order.setCar(car.getSelectionModel().getSelectedItem());
                        order.setClient(client.getSelectionModel().getSelectedItem());
                        order.setStartDate(Date.from(startDate.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
                        order.setEndDate(Date.from(endDate.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
                        if (orderDAO.updateEntry(order)) {
                            orderTable.setItems(FXCollections.observableArrayList(orderDAO.getAllEntries()));
                            CustomAlert.info("The order is updated successfully.");
                            clearForm();
                        } else {
                            CustomAlert.error("Some error occurred.");
                        }
                    } else {
                        Order order = new Order(
                                Date.from(startDate.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()),
                                Date.from(endDate.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()),
                                Double.valueOf(price.getText()),
                                client.getSelectionModel().getSelectedItem(),
                                car.getSelectionModel().getSelectedItem()
                        );
                        if (orderDAO.addEntry(order)) {
                            orderTable.setItems(FXCollections.observableArrayList(orderDAO.getAllEntries()));
                            CustomAlert.info("The order is added to the database");
                            clearForm();
                        } else {
                            CustomAlert.error("Some error occurred.");
                        }
                    }
                }
            } else {
                CustomAlert.error("Please fill all required fields.");
            }
        });

        clearButton.setOnAction(event -> clearForm());
        GUIUtilities.addMenuSupport(this);
    }

    private void clearForm() {
        client.valueProperty().setValue(null);
        car.valueProperty().setValue(null);
        startDate.getEditor().clear();
        endDate.getEditor().clear();
        price.clear();
    }

    @Override
    public Stage getStage() {
        return (Stage) this.clearButton.getScene().getWindow();
    }
}
