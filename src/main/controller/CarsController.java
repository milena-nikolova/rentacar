package main.controller;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import main.model.Car;
import main.model.dao.CarDAO;
import main.util.CustomAlert;
import main.util.GUIUtilities;

import java.net.URL;
import java.util.ResourceBundle;

public class CarsController extends Controller {
    private CarDAO carDAO = new CarDAO();
    private int carID = 0;

    @FXML
    private TableView<Car> carTable;
    @FXML
    private TableColumn<Car, String> modelCol;
    @FXML
    private TableColumn<Car, Integer> yearCol;
    @FXML
    private TableColumn<Car, String> licensePlateCol;
    @FXML
    private TableColumn<Car, Integer> seatsCol;
    @FXML
    private TableColumn<Car, Boolean> hasTrunkCol;
    @FXML
    private TableColumn<Car, Boolean> isCheckedCol;
    @FXML
    private TableColumn<Car, Car> editCol;
    @FXML
    private TableColumn<Car, Car> deleteCol;

    @FXML
    private TextField licensePlate;
    @FXML
    private TextField model;
    @FXML
    private TextField year;
    @FXML
    private ComboBox<Integer> seats;
    @FXML
    private CheckBox hasTrunk;
    @FXML
    private CheckBox isChecked;
    @FXML
    private Button saveButton;
    @FXML
    private Button clearButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        /* ------------------------------- Initialize and populate the table ---------------------------------------- */
        modelCol.setCellValueFactory(new PropertyValueFactory<>("model"));
        yearCol.setCellValueFactory(new PropertyValueFactory<>("year"));
        licensePlateCol.setCellValueFactory(new PropertyValueFactory<>("licensePlate"));
        seatsCol.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getSeats()));
        hasTrunkCol.setCellValueFactory(cellData -> new SimpleBooleanProperty(cellData.getValue().hasTrunk()));
        isCheckedCol.setCellValueFactory(cellData -> new SimpleBooleanProperty(cellData.getValue().isChecked()));

        editCol.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));

        editCol.setCellFactory(param -> new TableCell<Car, Car>() {
            private final Button editButton = new Button("Edit");

            @Override
            protected void updateItem(Car car, boolean empty) {
                super.updateItem(car, empty);

                if (car == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(editButton);
                editButton.setOnAction(event -> {
                    carID = car.getId();
                    licensePlate.setText(car.getLicensePlate());
                    year.setText(String.valueOf(car.getYear()));
                    model.setText(car.getModel());
                    seats.getSelectionModel().select(car.getSeats() - 3);
                    isChecked.setSelected(car.isChecked());
                    hasTrunk.setSelected(car.hasTrunk());
                });
            }
        });
        deleteCol.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));

        deleteCol.setCellFactory(param -> new TableCell<Car, Car>() {
            private final Button deleteButton = new Button("Delete");

            @Override
            protected void updateItem(Car car, boolean empty) {
                super.updateItem(car, empty);

                if (car == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(deleteButton);
                deleteButton.setOnAction(event -> {
                    getTableView().getItems().remove(car);
                    if (carDAO.deleteEntry(car.getId())) {
                        CustomAlert.info("The car was deleted successfully.");
                    } else {
                        CustomAlert.error("Some error occurred.");
                    }
                });
            }
        });


        carTable.setItems(FXCollections.observableArrayList(carDAO.getAllEntries()));
        /* ---------------------------------------------------------------------------------------------------------- */

        /* --------------------------------------- Initialize data fields ------------------------------------------- */
        GUIUtilities.setMaxLenght(year, 4);
        GUIUtilities.setMaxLenght(model, 50);
        GUIUtilities.setMaxLenght(licensePlate, 10);
        GUIUtilities.acceptIntegersOnly(year);

        seats.getItems().addAll(3, 4, 5, 6, 7, 8, 9);
        /* ---------------------------------------------------------------------------------------------------------- */

        clearButton.setOnAction(event -> clearForm());

        saveButton.setOnAction(event -> {
            if ((licensePlate.getText() != null && !licensePlate.getText().isEmpty()) &&
                    (model.getText() != null && !model.getText().isEmpty()) &&
                    (year.getText() != null && !year.getText().isEmpty()) &&
                    (seats.getValue() != null)) {

                if (!carDAO.checkLicensePlate(licensePlate.getText(), carID)) {
                    CustomAlert.info("A car with the same license plate is already registered.");
                } else if (!licensePlate.getText().matches("^[A-Z]{1,2}\\s\\d{4}\\s[A-Z]{2}")) {
                    CustomAlert.error("Please fill your licence plate in the specified format.");
                } else if (Integer.valueOf(year.getText()) < 1900 || Integer.valueOf(year.getText()) > 2017) {
                    CustomAlert.error("The year is wrong.");
                } else {
                    if (carID == 0) {
                        Car car = new Car(
                                model.getText(),
                                Integer.valueOf(year.getText()),
                                licensePlate.getText(),
                                seats.getSelectionModel().getSelectedItem(),
                                hasTrunk.isSelected(),
                                isChecked.isSelected());
                        if (carDAO.addEntry(car)) {
                            carTable.setItems(FXCollections.observableArrayList(carDAO.getAllEntries()));
                            CustomAlert.info("The entry is added to the database");
                            clearForm();
                        } else {
                            CustomAlert.error("The car was not created.");
                        }
                    } else {
                        Car car = carDAO.getEntry(carID);
                        car.setModel(model.getText());
                        car.setYear(Integer.valueOf(year.getText()));
                        car.setLicensePlate(licensePlate.getText());
                        car.setChecked(isChecked.isSelected());
                        car.setHasTrunk(hasTrunk.isSelected());
                        car.setSeats(seats.getSelectionModel().getSelectedItem());
                        carDAO.updateEntry(car);
                        if (carDAO.updateEntry(car)) {
                            carTable.setItems(FXCollections.observableArrayList(carDAO.getAllEntries()));
                            CustomAlert.info("The entry has been updated");
                            clearForm();
                            carID = 0;
                        } else {
                            CustomAlert.error("The car was not updated.");
                        }
                    }

                }
            } else {
                CustomAlert.error("Please fill all required fields.");
            }
        });
        GUIUtilities.addMenuSupport(this);
    }

    private void clearForm() {
        licensePlate.clear();
        model.clear();
        seats.getSelectionModel().clearSelection();
        year.clear();
        hasTrunk.setSelected(false);
        isChecked.setSelected(false);
    }

    @Override
    public Stage getStage() {
        return (Stage) this.clearButton.getScene().getWindow();
    }
}
