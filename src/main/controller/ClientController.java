package main.controller;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import main.model.Client;
import main.model.DrivingLicense;
import main.model.dao.ClientDAO;
import main.util.CustomAlert;
import main.util.GUIUtilities;

import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;

public class ClientController extends Controller {
    private ClientDAO clientDAO = new ClientDAO();
    private int clientID = 0;

    @FXML
    private TableView<Client> clientTable;
    @FXML
    private TableColumn<Client, String> firstNameCol;
    @FXML
    private TableColumn<Client, String> lastNameCol;
    @FXML
    private TableColumn<Client, String> egnCol;
    @FXML
    private TableColumn<Client, String> phoneCol;
    @FXML
    private TableColumn<Client, String> addressCol;
    @FXML
    private TableColumn<Client, String> licenseNumberCol;
    @FXML
    private TableColumn<Client, Date> dateIssuedCol;
    @FXML
    private TableColumn<Client, Date> dateExpiresCol;
    @FXML
    private TableColumn<Client, Client> editCol;
    @FXML
    private TableColumn<Client, Client> deleteCol;

    @FXML
    private TextField firstName;
    @FXML
    private TextField lastName;
    @FXML
    private TextField egn;
    @FXML
    private TextField phone;
    @FXML
    private TextField address;
    @FXML
    private TextField licenseNumber;
    @FXML
    private DatePicker dateIssued;
    @FXML
    private DatePicker dateExpires;
    @FXML
    private Button saveButton;
    @FXML
    private Button clearButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        /* ------------------------------- Initialize and populate the table ---------------------------------------- */
        firstNameCol.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        egnCol.setCellValueFactory(new PropertyValueFactory<>("egn"));
        phoneCol.setCellValueFactory(new PropertyValueFactory<>("phone"));
        addressCol.setCellValueFactory(new PropertyValueFactory<>("address"));
        licenseNumberCol.setCellValueFactory(param -> new SimpleStringProperty(String.valueOf(param.getValue().getLicense().getLicenseNumber())));
        dateIssuedCol.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getLicense().getDateIssued()));
        dateExpiresCol.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getLicense().getDateExpires()));
        editCol.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        editCol.setCellFactory(param -> new TableCell<Client, Client>() {
            private final Button editButton = new Button("Edit");

            @Override
            protected void updateItem(Client client, boolean empty) {
                super.updateItem(client, empty);

                if (client == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(editButton);
                editButton.setOnAction(event -> {
                    clientID = client.getId();
                    firstName.setText(client.getAddress());
                    lastName.setText(client.getLastName());
                    egn.setText(client.getEgn());
                    address.setText(client.getAddress());
                    phone.setText(client.getPhone());
                    licenseNumber.setText(client.getLicense().getLicenseNumber());
                    dateIssued.setValue(GUIUtilities.toLocalDate(client.getLicense().getDateIssued().toString()));
                    dateExpires.setValue(GUIUtilities.toLocalDate(client.getLicense().getDateExpires().toString()));
                });
            }
        });
        deleteCol.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        deleteCol.setCellFactory(param -> new TableCell<Client, Client>() {
            private final Button deleteButton = new Button("Delete");

            @Override
            protected void updateItem(Client client, boolean empty) {
                super.updateItem(client, empty);

                if (client == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(deleteButton);
                deleteButton.setOnAction(event -> {
                    getTableView().getItems().remove(client);
                    if (clientDAO.deleteEntry(client.getId())) {
                        CustomAlert.info("The selected client was deleted successfully.");
                    } else {
                        CustomAlert.error("Some error occurred.");
                    }
                });
            }
        });

        clientTable.setItems(FXCollections.observableArrayList(clientDAO.getAllEntries()));
        /* ---------------------------------------------------------------------------------------------------------- */

        GUIUtilities.setMaxLenght(firstName, 50);
        GUIUtilities.setMaxLenght(lastName, 50);
        GUIUtilities.setMaxLenght(egn, 10);
        GUIUtilities.setMaxLenght(phone, 15);
        GUIUtilities.setMaxLenght(address, 255);
        GUIUtilities.setMaxLenght(licenseNumber, 8);
        GUIUtilities.acceptIntegersOnly(licenseNumber);
        GUIUtilities.acceptStringsOnly(firstName);
        GUIUtilities.acceptStringsOnly(lastName);
        GUIUtilities.acceptIntegersOnly(egn);

        saveButton.setOnAction(event -> {
            if ((firstName.getText() != null && !firstName.getText().isEmpty()) &&
                    (lastName.getText() != null && !lastName.getText().isEmpty()) &&
                    (phone.getText() != null && !phone.getText().isEmpty()) &&
                    (egn.getText() != null && !egn.getText().isEmpty()) &&
                    (address.getText() != null && !address.getText().isEmpty()) &&
                    (licenseNumber.getText() != null && !licenseNumber.getText().isEmpty()) &&
                    (dateIssued.getValue() != null && dateExpires.getValue() != null)) {

                if (!clientDAO.checkEGN(egn.getText(), clientID)) {
                    CustomAlert.info("A car with the same license plate is already registered.");
                } else if (!phone.getText().matches("^\\+[1-9][0-9]{3,14}$")) {
                    CustomAlert.error("Please fill the phone number in the specified format.");
                } else if (egn.getLength() != 10) {
                    CustomAlert.error("EGN is wrong");
                } else if (dateIssued.getValue().isAfter(dateExpires.getValue()) || dateIssued.getValue().isAfter(LocalDate.now())) {
                    CustomAlert.error("Check the dates for the driving license.");
                }
                else {
                    if (clientID > 0) {
                        Client client = clientDAO.getEntry(clientID);
                        client.setFirstName(firstName.getText());
                        client.setLastName(lastName.getText());
                        client.setEgn(egn.getText());
                        client.setAddress(address.getText());
                        client.setPhone(phone.getText());
                        client.getLicense().setLicenseNumber(licenseNumber.getText());
                        client.getLicense().setDateIssued(Date.from(dateIssued.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
                        client.getLicense().setDateExpires(Date.from(dateExpires.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()));
                        if (clientDAO.updateEntry(client)) {
                            clientTable.setItems(FXCollections.observableArrayList(clientDAO.getAllEntries()));
                            CustomAlert.info("The entry is updated successfully.");
                            clearForm();
                        } else {
                            CustomAlert.error("Some error occurred.");
                        }
                    } else {
                        Client client = new Client(
                                firstName.getText(),
                                lastName.getText(),
                                egn.getText(),
                                phone.getText(),
                                address.getText(),
                                new DrivingLicense(
                                        licenseNumber.getText(),
                                        Date.from(dateIssued.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant()),
                                        Date.from(dateExpires.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant())
                                )
                        );
                        if (clientDAO.addEntry(client)) {
                            clientTable.setItems(FXCollections.observableArrayList(clientDAO.getAllEntries()));
                            CustomAlert.info("The entry is added to the database");
                            clearForm();
                        } else {
                            CustomAlert.error("Some error occurred.");
                        }
                    }
                }
            } else {
                CustomAlert.error("Please fill all required fields.");
            }
        });

        clearButton.setOnAction(event -> clearForm());
        GUIUtilities.addMenuSupport(this);
    }

    private void clearForm() {
        firstName.clear();
        lastName.clear();
        egn.clear();
        phone.clear();
        address.clear();
        licenseNumber.clear();
        dateIssued.getEditor().clear();
        dateExpires.getEditor().clear();
    }

    @Override
    public Stage getStage() {
        return (Stage) this.clearButton.getScene().getWindow();
    }
}
