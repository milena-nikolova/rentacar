package main.controller;

import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
import main.model.Client;
import main.model.dao.ClientDAO;
import main.model.dao.OrderDAO;
import main.util.GUIUtilities;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ClientStatsController extends Controller {
    @FXML
    private BarChart barChart;
    @FXML
    private NumberAxis numberAxis;
    @FXML
    private CategoryAxis categoryAxis;

    @Override
    public Stage getStage() {
        return (Stage) barChart.getScene().getWindow();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        barChart.setTitle("Client Activity Statistics");
        numberAxis.setLabel("Number of orders");
        numberAxis.setTickLabelRotation(90);
        categoryAxis.setLabel("Client");
        OrderDAO orderDAO = new OrderDAO();
        ArrayList<Client> allClients = new ClientDAO().getAllEntries();
        XYChart.Series series = new XYChart.Series();
        series.setName("Number of orders made by client");

        for (Client client : allClients) {
            series.getData().add(new XYChart.Data(
                    client.toString(),
                    Integer.valueOf(orderDAO.getOrdersCountForClient(client.getId()))
            ));
        }

        barChart.getData().addAll(series);

        GUIUtilities.addMenuSupport(this);
    }
}
