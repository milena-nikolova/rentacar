package main.controller;

import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
import main.model.Car;
import main.model.dao.CarDAO;
import main.model.dao.OrderDAO;
import main.util.GUIUtilities;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class OrderStatsController extends Controller {
    @FXML
    private BarChart barChart;
    @FXML
    private NumberAxis numberAxis;
    @FXML
    private CategoryAxis categoryAxis;

    @Override
    public Stage getStage() {
        return (Stage) barChart.getScene().getWindow();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        barChart.setTitle("Car price comparison");
        numberAxis.setLabel("Price");
        numberAxis.setTickLabelRotation(90);
        categoryAxis.setLabel("Car");
        OrderDAO orderDAO = new OrderDAO();
        ArrayList<Car> allClients = new CarDAO().getAllEntries();
        XYChart.Series series = new XYChart.Series();
        series.setName("Price of used Cars");

        for (Car car : allClients) {
            series.getData().add(new XYChart.Data(
                    Double.valueOf(orderDAO.getAveragePriceForCar(car.getId())),car.toString()
            ));
        }

        barChart.getData().addAll(series);

        GUIUtilities.addMenuSupport(this);
    }
}
