package main.controller;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import main.model.Client;
import main.model.dao.ClientDAO;
import main.util.GUIUtilities;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

public class ClientFilterController extends Controller {
    private ClientDAO clientDAO = new ClientDAO();

    @FXML
    private TableView<Client> clientTable;
    @FXML
    private TableColumn<Client, String> firstNameCol;
    @FXML
    private TableColumn<Client, String> lastNameCol;
    @FXML
    private TableColumn<Client, String> egnCol;
    @FXML
    private TableColumn<Client, String> phoneCol;
    @FXML
    private TableColumn<Client, String> addressCol;
    @FXML
    private TableColumn<Client, String> licenseNumberCol;
    @FXML
    private TableColumn<Client, Date> dateIssuedCol;
    @FXML
    private TableColumn<Client, Date> dateExpiresCol;

    @FXML
    private TextField firstName;
    @FXML
    private TextField lastName;
    @FXML
    private TextField egn;
    @FXML
    private TextField licenseNumber;
    @FXML
    private Button searchButton;
    @FXML
    private Button clearButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        firstNameCol.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        egnCol.setCellValueFactory(new PropertyValueFactory<>("egn"));
        phoneCol.setCellValueFactory(new PropertyValueFactory<>("phone"));
        addressCol.setCellValueFactory(new PropertyValueFactory<>("address"));
        licenseNumberCol.setCellValueFactory(param -> new SimpleStringProperty(String.valueOf(param.getValue().getLicense().getLicenseNumber())));
        dateIssuedCol.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getLicense().getDateIssued()));
        dateExpiresCol.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getLicense().getDateExpires()));

        GUIUtilities.setMaxLenght(firstName, 50);
        GUIUtilities.setMaxLenght(lastName, 50);
        GUIUtilities.setMaxLenght(egn, 10);
        GUIUtilities.setMaxLenght(licenseNumber, 8);

        GUIUtilities.acceptIntegersOnly(egn);
        GUIUtilities.acceptIntegersOnly(licenseNumber);
        GUIUtilities.acceptStringsOnly(firstName);
        GUIUtilities.acceptStringsOnly(lastName);

        searchButton.setOnAction(event -> {
            if (firstName.getText() != null && lastName.getText() != null && egn.getText() != null && licenseNumber.getText() != null) {
                clientTable.setItems(FXCollections.observableArrayList(
                        clientDAO.filterEntries(
                                !firstName.getText().isEmpty() ? firstName.getText() : "",
                                !lastName.getText().isEmpty() ? lastName.getText() : "",
                                !egn.getText().isEmpty() ? firstName.getText() : "",
                                !licenseNumber.getText().isEmpty() ? Integer.valueOf(lastName.getText()) : 0
                        )
                ));
            }
        });

        clearButton.setOnAction(event -> {
            clearForm();
            clientTable.setItems(null);
        });

        GUIUtilities.addMenuSupport(this);
    }

    private void clearForm() {
        firstName.clear();
        lastName.clear();
        egn.clear();
        licenseNumber.clear();
    }

    @Override
    public Stage getStage() {
        return (Stage) this.clearButton.getScene().getWindow();
    }
}
