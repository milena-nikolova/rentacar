package main.util;

import javafx.scene.control.Alert;

public class CustomAlert {
    public static void error(String message) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setHeaderText("Error!");
        dialog.setContentText(message);
        dialog.showAndWait();
    }

    public static void info(String message) {
        Alert dialog = new Alert(Alert.AlertType.INFORMATION);
        dialog.setHeaderText("Information!");
        dialog.setContentText(message);
        dialog.showAndWait();
    }
}
