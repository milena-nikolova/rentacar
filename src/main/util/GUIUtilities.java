package main.util;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.controller.Controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class GUIUtilities {

    public static LocalDate toLocalDate(String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(dateString, formatter);
        return localDate;
    }

    public static void setMaxLenght(final TextField tf, final int maxLength) {
        tf.textProperty().addListener((ov, oldValue, newValue) -> {
            if (tf.getText().length() > maxLength) {
                String s = tf.getText().substring(0, maxLength);
                tf.setText(s);
            }
        });
    }

    public static void acceptIntegersOnly(final TextField tf) {
        tf.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                tf.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
    }

    public static void acceptDoubleOnly(final TextField tf) {
        tf.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d+\\.\\d+")) {
                tf.setText(newValue.replaceAll("[^\\d|(\\d+.\\d+)]", ""));
            }
        });
    }

    public static void acceptStringsOnly(final TextField tf) {
        tf.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("[A-Za-z\\s]*")) {
                tf.setText(newValue.replaceAll("[^A-Za-z\\s]", ""));
            }
        });
    }

    public static boolean isNumeric(String text) {
        try {
            Double.parseDouble(text);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static void addMenuSupport(Controller controller) {
        controller.carsMenuItem.setOnAction(event -> {
            Stage stage;

            stage = controller.getStage();
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(controller.getClass().getResource("../view/car.fxml"));
                Parent content = loader.load();

                Scene scene = new Scene(content);
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        controller.clientsMenuItem.setOnAction(event -> {
            Stage stage;

            stage = controller.getStage();
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(controller.getClass().getResource("../view/client.fxml"));
                Parent content = loader.load();

                Scene scene = new Scene(content);
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        controller.ordersMenuItem.setOnAction(event -> {
            Stage stage;

            stage = controller.getStage();
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(controller.getClass().getResource("../view/order.fxml"));
                Parent content = loader.load();

                Scene scene = new Scene(content);
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        controller.filterMenuItem.setOnAction(event -> {
            Stage stage;

            stage = controller.getStage();
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(controller.getClass().getResource("../view/orderfilter.fxml"));
                Parent content = loader.load();

                Scene scene = new Scene(content);
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        controller.filterCarsMEnuItem.setOnAction(event -> {
            Stage stage;

            stage = controller.getStage();
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(controller.getClass().getResource("../view/carfilter.fxml"));
                Parent content = loader.load();

                Scene scene = new Scene(content);
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        controller.filterClientsMenuItem.setOnAction(event -> {
            Stage stage;

            stage = controller.getStage();
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(controller.getClass().getResource("../view/clientfilter.fxml"));
                Parent content = loader.load();

                Scene scene = new Scene(content);
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        controller.carStatsMenuItem.setOnAction(event -> {
            Stage stage;

            stage = controller.getStage();
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(controller.getClass().getResource("../view/carstats.fxml"));
                Parent content = loader.load();

                Scene scene = new Scene(content);
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        controller.clientStatsMenuItem.setOnAction(event -> {
            Stage stage;

            stage = controller.getStage();
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(controller.getClass().getResource("../view/clientstats.fxml"));
                Parent content = loader.load();

                Scene scene = new Scene(content);
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        controller.orderStatsMenuItem.setOnAction(event -> {
            Stage stage;

            stage = controller.getStage();
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(controller.getClass().getResource("../view/orderstats.fxml"));
                Parent content = loader.load();

                Scene scene = new Scene(content);
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
